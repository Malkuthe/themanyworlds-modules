Hello and welcome to the modules repository for the Shardscape Modules!

# Shardscape Modules
Shardscape Modules is meant to be a place to submit, store, and host tabletop roleplaying game modules. These modules may indeed be compatible with any system, say 3.5th edition for example, or maybe even 5th.

Hang on for just a moment as I'm still setting up the structure for this website. However, do check back in every once in a while and see if any changes have been made. I'll be updating this as I go. Once I'm done setting up the framework for the website, I'll be making a sample module just so people know how to do things around here.

In the meantime, hang tight!